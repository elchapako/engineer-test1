<?php

namespace App\Http\Controllers;

use App\ProcessMaker\ProcessMaker;
use Illuminate\Http\Request;
use Exception;
use Cache;
use Faker\Generator;
use stdClass;

class UsersController extends Controller
{
	public function index() {
        $pmServer = "https://poc-1.processmaker.net";

        $ch = curl_init($pmServer . "/api/1.0/workflow/users");

        $token = app()->make(\App\ProcessMaker\ProcessMaker::class)->getAccessToken();

        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Authorization: Bearer " . $token));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $aUsers = json_decode(curl_exec($ch));
        $statusCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if ($statusCode != 200) {
            if (isset ($aUsers) and isset($aUsers->error))
                print "Error code: {$aUsers->error->code}\nMessage: {$aUsers->error->message}\n";
            else
                print "Error: HTTP status code: $statusCode\n";
        }
        else {
            foreach ($aUsers as $oUser) {
                if ($oUser->usr_status == "ACTIVE") {
                    //print "{$oUser->usr_firstname} {$oUser->usr_lastname} ({$oUser->usr_username})\n";
                }
            }
        }
        return ["data" => $aUsers];
        //return view('welcome', compact('aUsers'));
	}

	public function approve() {
        $curl = curl_init();
        $token = app()->make(\App\ProcessMaker\ProcessMaker::class)->getAccessToken();
        curl_setopt_array($curl, array(
            CURLOPT_PORT => env('PROCESSMAKER_PORT'),
            CURLOPT_URL => env('PROCESSMAKER_URL') . "/api/1.0/workflow/plugin-test1/pass",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer " . $token,
                "cache-control: no-cache",
                "content-type: application/json"
            ),
        ));

        $response = curl_exec($curl);

        $err = curl_error($curl);

        curl_close($curl);

        return ["data" => json_decode($response)];
	}

}
